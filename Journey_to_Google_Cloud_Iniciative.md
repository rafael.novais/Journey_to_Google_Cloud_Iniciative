# Journey to Google Cloud Iniciative
---

This document serves as a guide to new joiners on Google Cloud Iniciative. 

You will be in touch with concepts like:  
- [DevOps](#devops)  

and technologies such as:  
- [Google-Cloud-Platform](#google-cloud-platform)
- [Terraform](#terraform)
- [Kubernetes](#kubernetes)
- [Docker](#docker)
- [Jenkins](#jenkins)
- [GitLab](#gitlab)
- [Apigee](#apigee)

To go with those, you are provided with some [Practice Tutorials](#practice-tutorials).  
You can do those as you read through technologies. 

In the end, you are provided with [Additional Links](#additional-links) where you can find more information.  
We advise you to search through them on subjects you may not yet feel comfortable.  
Just be aware that many of these resources have repeated information.  

Good luck and have fun!


# Concepts
---
## DevOps

- [ ]  [DevOps Introduction Tutorial - javatpoint](https://www.javatpoint.com/devops)

# Tools
---
## Google Cloud Platform

- [ ]  [Migration Strategies](https://txture.io/en/blog/6-Rs-cloud-migration-strategies)
- [ ]  [Cloud migration: What you need to know](https://cloud.google.com/blog/products/cloud-migration/guide-to-all-google-cloud-migration-guides)
- [ ]  [Cloud migration: Getting started](https://cloud.google.com/solutions/migration-to-gcp-getting-started)
- [ ]  [Google Cloud - Percipio](https://accenture.percipio.com/channels/62ad1441-12ff-11e7-822f-df9d5d5ce619)

## Terraform  

- [ ]  [Comprehensive Guide to Terraform](https://blog.gruntwork.io/a-comprehensive-guide-to-terraform-b3d32832baca)

## Kubernetes  

- [ ]  [Kubernetes Tutorial for Beginners](https://www.youtube.com/watch?v=X48VuDVv0do)


## Docker 
- [ ]  [Docker 101](https://github.com/docker/labs/tree/master/beginner/)
- [ ]  [Dockerizing a Node.js application](https://github.com/docker/labs/tree/master/developer-tools/nodejs/porting/)

## Jenkins



## GitLab

## Apigee

- [ ]  [API Management and DevOps](https://accenture.percipio.com/courses/729ff32e-81c2-489d-a3f7-844396094109/videos/109e70c9-15ba-444d-9f4c-e039e95ac4f9)

## Practice Tutorials


- [ ]  [Deploy a Spring Boot Java app to Kubernetes on Google Kubernetes Engine](https://codelabs.developers.google.com/codelabs/cloud-springboot-kubernetes#0)
- [ ]  [Deploy, scale, and update your website with Google Kubernetes Engine](https://codelabs.developers.google.com/codelabs/cloud-deploy-website-on-gke#0) 
- [ ]  [Migrating a Monolithic Website to Microservices on Google Kubernetes Engine](https://codelabs.developers.google.com/codelabs/cloud-monolith-to-microservices-gke#0)  
- [ ]  [Terraform on Google Cloud](https://cloud.google.com/community/tutorials/getting-started-on-gcp-with-terraform)
- [ ]  [Setting up a Jenkins Server in a GKE Cluster](https://cloud.google.com/architecture/jenkins-on-kubernetes-engine-tutorial)  


# Additional Links
---

- [ ]  [Google Cloud Architecture Center](https://cloud.google.com/architecture)
- [ ]  [Google Codelabs](https://codelabs.developers.google.com/)
- [ ]  [Percipio](https://accenture.percipio.com/)
- [ ]  [FreeCodeCamp youtube channel](https://www.youtube.com/c/Freecodecamp/videos)
- [ ]  [Jenkins Tutorials](https://www.jenkins.io/doc/tutorials/)


## TODO:

Add concepts: 
    Public Cloud,
    Private CLoud,
    PAAS, IAAS, SAAS,
    API, Microservice,
